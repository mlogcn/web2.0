/**
 * Created by fuchao on 15/8/3.
 */
/*global $*/
$(function () {
  'use strict';
  $.extend({
    'config': {
      apiHost: 'http://dev.api.mlogcn.com:8000',
      apiCurrentWeahter: '/api/wicon/v2/ob/wx/coor/',
      api24hData: '/api/wicon/v2/fc/wx/24h/coor/',
      apiWeekSummary: '/api/wicon/v2/summary/plot/coor/',
      apiHourly: '/api/wicon/v2/hourly/plot/coor/',
      apiNearly2h: '/api/wicon/v2/nc/coor/',
      apiAqiDuring: 'api/wicon/v2/aqi/coor/',
      apiSearch: '/api/wicon/v1/area/serach?area='
    },
    'constent': {
      contentType: 'application/x-www-form-urlencoded;charset=utf-8'
    }
  });
});
