/**
 * Created by fuchao on 15/8/3.
 */
/*global $*/
$(function () {
  'use strict';
  $.extend({
    'sayHello': function () {
      console.log('%c欢迎到天气家公众账号翻我的牌子,  fuchao is waiting for u.', 'background-image:-webkit-gradient( linear,  left top,  right top,  color-stop(0,  #f22),  color-stop(0.15,  #f2f),  color-stop(0.3,  #22f),  color-stop(0.45,  #2ff),  color-stop(0.6,  #2f2), color-stop(0.75,  #2f2),  color-stop(0.9,  #ff2),  color-stop(1,  #f22) );color:transparent;-webkit-background-clip: text;font-size:5em;');
    },
    'weatherAjax': function (url, callback) {
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: $.constent.contentType,
        error: function () {
          console.log('wicon ajax error');
        },
        success: callback
      });
    },
    'searchAjax': function (url, beforesend, callback) {
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: $.constent.contentType,
        beforeSend: beforesend,
        success: callback
      });
    },
    'extremum': function (numArray, max) {
      max = max || 'max';
      return max === 'max' ? (Math.max.apply(null, numArray)) : (Math.min.apply(null, numArray));
    },
    'fixAqi': function (aqi) {
      return aqi.length > 2 ? aqi : '空气' + aqi;
    },
    'noiseAqi': function (min) {
      var max = min + 3;
      min = min - 3;
      return Math.ceil(Math.random() * (max - min) + min);
    },
    'sprintf': function () {
      var arg = arguments,
        str = arg[0] || '',
        i, n;
      for (i = 1, n = arg.length; i < n; i++) {
        str = str.replace(/%s/, arg[i]);
      }
      return str;
    },
    'getGeo': function (latitude, longtitude) {
      $.weatherAjax($.sprintf('http://api.map.baidu.com/geocoder/v2/?ak=%s&callback=?&location=%s, %s&output=json', 'HiggSOF8itz4VGPAYfe1rSOl', latitude, longtitude), function (data) {
        if (data !== 'undefined') {
          return data;
        }
      });
    },
    'getRequest': function () {
      var url = location.search, strs = [];
      var theRequest = {};
      if (url.indexOf('?') !== -1) {
        var str = url.substr(1);
        strs = str.split('&');
        for (var i = 0; i < strs.length; i++) {
          theRequest[strs[i].split('=')[0]] = decodeURI(strs[i].split('=')[1]).trim();
        }
      }
      console.log(theRequest);
      return theRequest;
    },
    'releaseFormat': function (t) {
      //'2015080320'
      return t.substr(4, 2) + '/' + t.substr(6, 2) + ' ' + t.substr(8, 2) + ':00 发布';
    },
    'weekTimeFormat': function (t) {
      var mon, day;
      mon = t.substr(4, 2);
      day = t.substr(6, 2);
      return mon + '/' + day;
    },
    'getDayOfWeek': function (num) {
      var date = (new Date()).getDay(),
        day = '', test = (date + num >= 7) ? (date + num - 7) : (date + num);
      switch (test) {
        case 0:
          day = '周日';
          break;
        case 1:
          day = '周一';
          break;
        case 2:
          day = '周二';
          break;
        case 3:
          day = '周三';
          break;
        case 4:
          day = '周四';
          break;
        case 5:
          day = '周五';
          break;
        case 6:
          day = '周六';
          break;
        default:
          day = 'N/A';
          break;
      }
      return day;
    }
  });
});
