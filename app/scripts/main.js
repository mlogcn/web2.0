/*global $*/
'use strict';
$(function () {
  /**
   * event start
   */
  var CLICK = 'click';
  if (/(iPhone|iPad|iOS)/i.test(navigator.userAgent)) {
    CLICK = 'touchstart';
  }
  $('span#currentCity').on(CLICK, function () {
    var positionPage = $('#positionPage');
    $.hotCity();
    positionPage.css({'top': '160px', 'display': 'block', 'visibility': 'visible'});
    positionPage.animate({
      width: '100%',
      opacity: 1,
      top: '+=50px'
    }, 500);
  });
  $('.current-city,.hotcitys').bind(CLICK, function () {
    $('#positionPage').animate({
      width: '0',
      opacity: 0,
      top: '-=50px'
    }, 500);
  });
  $('.city-list ul li a,.search-result ul li a,.current-city').bind('touchstart', function () {
    $(this).addClass('touch-active');
  }).bind('touchend', function () {
    $(this).removeClass('touch-active');
  });
  var inputCityName = $('#inputCityName');
  inputCityName.bind('keyup', function () {
    var jqueryInput = $(this);
    var searchText = jqueryInput.val();
    var html = '', cityName;
    if ((inputCityName.val() === undefined) || (inputCityName.val().length === 0)) {
      return;
    }
    $.searchAjax($.config.apiHost + $.config.apiSearch + searchText,
      function () {
        $('#loading').show();
      },
      function (data) {
        $('#loading').hide();
        if (data.length > 0) {
          for (var i = data.length; i > 0; i--) {
            cityName = data[i - 1].city + ' ' + data[i - 1].county;
            html += $.sprintf('<li class="hotcitys"><a href="/index.html?area=%s">%s</a></li>', cityName, cityName);
          }
        } else {
          html = '<li class="hotcitys"><a href="javascript:">无匹配城市</a></li>';
        }
        $('#soResult').html(html);
        $('#resultPanel').css({
          visibility: 'visible',
          display: 'block'
        });
      });
  });
});
$(function () {
  //$.sayHello();
  var Expand = function () {
    var tile = $('.strips__strip');
    var tileLink = $('.strips__strip > .strip__content');
    var tileText = tileLink.find('.strip__inner-text');
    var stripClose = $('.strip__close');
    var expanded = false;
    var open = function () {
      var tiles = $(this).parent();
      if (!expanded) {
        tiles.addClass('strips__strip--expanded');
        tileText.css('transition', 'all .6s .5s cubic-bezier(0.23, 1, 0.32, 1)');
        stripClose.addClass('strip__close--show');
        stripClose.css('transition', 'all .6s .5s cubic-bezier(0.23, 1, 0.32, 1)');
        expanded = true;
      }
    };
    var close = function () {
      if (expanded) {
        tile.removeClass('strips__strip--expanded');
        tileText.css('transition', 'all 0.05s 0 cubic-bezier(0.23, 1, 0.32, 1)');
        stripClose.removeClass('strip__close--show');
        stripClose.css('transition', 'all 0.2s 0s cubic-bezier(0.23, 1, 0.32, 1)');
        expanded = false;
      }
    };
    var bindActions = function () {
      tileLink.on('click', open);
      stripClose.on('click', close);
    };
    var init = function () {
      bindActions();
    };
    return {init: init};
  }();
  Expand.init();
  $('#gopng_home_contact_wechat_gray').on('mouseenter', function () {
      $('.weixin-logo').show();
  }).on('mouseleave', function () {
    $('.weixin-logo').hide();
  });
  $('#app-qr').on('mouseover', function () {
      $('.app-download').show();
  }).on('mouseleave', function () {
    $('.app-download').hide();
  });
});
$(function () {
  $('#nav-bar').on('click', function (e) {
    $(e.target).addClass('active').siblings('.active').removeClass('active');
  });
});
