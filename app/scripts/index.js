/*global AMap $ jQuery*/

/*utils*/
$(function () {
  'use strict';
  $.extend({
    'weatherAjax': function (url, callback) {
      jQuery.support.cors = true;
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: $.constent.contentType,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('weather ajax error');
          console.log(XMLHttpRequest.status);
          console.log(XMLHttpRequest.readyState);
          console.log(textStatus);
          console.log(errorThrown);
        },
        success: callback
      });
    },
    'searchAjax': function (url, beforesend, callback) {
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: $.constent.contentType,
        beforeSend: beforesend,
        success: callback
      });
    },
    'extremum': function (numArray, max) {
      max = max || 'max';
      return max === 'max' ? (Math.max.apply(null, numArray)) : (Math.min.apply(null, numArray));
    },
    'fixAqi': function (aqi) {
      return aqi.length > 2 ? aqi : '空气' + aqi;
    },
    'noiseAqi': function (min) {
      var max = min + 3;
      min = min - 3;
      return Math.ceil(Math.random() * (max - min) + min);
    },
    'setWeatherImg': function () {

    },
    'sprintf': function () {
      var arg = arguments,
        str = arg[0] || '',
        n = arg.length;
      for (var i = 1; i < n; i++) {
        str = str.replace(/%s/, arg[i]);
      }
      return str;
    },
    'getGeo': function (latitude, longtitude) {
      $.weatherAjax($.sprintf('http://api.map.baidu.com/geocoder/v2/?ak=%s&callback=?&location=%s, %s&output=json', 'HiggSOF8itz4VGPAYfe1rSOl', latitude, longtitude), function (data) {
        if (data !== 'undefined') {
          return data;
        }
      });
    },
    'changeCity': function (cityName) {
      $('.currentCity').html(cityName);
    },
    'releaseFormat': function (t) {
      //'2015080320'
      return t.substr(4, 2) + '/' + t.substr(6, 2) + ' ' + t.substr(8, 2) + ':00 由象辑科技发布';
    },
    'weekTimeFormat': function (t) {
      var mon = '',
        day = '',
        testTime,
        curDate = new Date();

      testTime = curDate.getFullYear();
      testTime += curDate.getMonth() > 8 ? (curDate.getMonth() + 1) : ('0' + (curDate.getMonth() + 1));
      testTime += curDate.getDate() > 9 ? curDate.getDate() : ('0' + curDate.getDate());
      if (t === testTime) {
        return '今天';
      } else if (t === testTime - 1 + '') {
        return '昨天';
      } else {
        mon = t.substr(4, 2);
        day = t.substr(6, 2);
        return mon + '/' + day;
      }
    }
  });
});

/*config*/
$(function () {
  'use strict';
  $.extend({
    'config': {
      apiHost: 'http://dev.api.mlogcn.com:8000',
      apiCurrentWeahter: '/api/weather/v2/ob/coor/',
      api24hData: '/api/weather/v2/fc/24h/coor/',
      apiWeekSummary: '/api/weather/v2/summary/plot/coor/',
      apiHourly: '/api/weather/v2/hourly/plot/coor/',
      apiNearly2h: '/api/weather/v2/nc/coor/',
      apiAqiDuring: 'api/weather/v2/aqi/coor/',
      apiSearch: '/api/weather/v1/area/serach?area='
    },
    'constent': {
      contentType: 'application/x-www-form-urlencoded;charset=utf-8'
    }
  });
});

/*geolocation*/
$(function () {
  'use strict'; //ES5
  //window.onload
  //$.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function () {
  //	console.log(remote_ip_info.city);
  //});
  function hourlyData(data, date) {
    //console.log(data);
    //todo @update data is valuable
    if (data.W_24h.length >= 169 && date) {
      var len = data.W_24h.length;
      var hourly = [];
      for (var i = 0; i < len; i++) {
        if (data.W_24h[i].time === date + '00') {
          for (var j = 0; j < 7; j++) {
            hourly[j] = data.W_24h.slice(i - 24 + 24 * j, i + 1 + 24 * j);
          }
          //console.log(hourly);
        }
      }
      //todo @update status value -> globe
      $.clickAction('daily', false, false, hourly);
    }
  }

  function geocoder(curCity) {
    var MGeocoder;
    //加载地理编码插件
    AMap.service(['AMap.Geocoder'], function () {
      MGeocoder = new AMap.Geocoder({
        radius: 1000 //范围，默认：500
      });
      //返回地理编码结果
      //地理编码
      MGeocoder.getLocation(curCity, function (status, result) {
        var geocode = {
          lng: null,
          lat: null
        };
        if (status === 'complete' && result.info === 'OK' && result.geocodes.length === 1) {
          geocode.lng = result.geocodes[0].location.getLng();
          geocode.lat = result.geocodes[0].location.getLat();
        } else {
          geocode.lng = 116.4;
          geocode.lat = 39.9;
        }
        var today = '',
          count = 3;
        var allDataSuccess = function () {
          count--;
          if (!count) {
            $('.carousel-weather-wrap').css('display', 'block');
          }
        };
        $.weatherAjax($.sprintf('%s%s/%s.json', $.config.apiHost + $.config.apiCurrentWeahter, geocode.lng, geocode.lat), function (now) {
          today = now.upTime.substr(0, 8);
          //console.log(today);
          $.renderNow(now);
          allDataSuccess();
        });
        $.weatherAjax($.sprintf('%s%s/%s.json', $.config.apiHost + $.config.apiWeekSummary, geocode.lng, geocode.lat), function (week) {
          //console.log(week);
          $.renderWeek(week);
          allDataSuccess();
        });
        $.weatherAjax($.sprintf('%s%s/%s.json', $.config.apiHost + $.config.apiHourly, geocode.lng, geocode.lat), function (hour) {
          hourlyData(hour, today);
          allDataSuccess();
        });
      });
    });
  }

//todo @update shipei ios need a space to init search
  if (decodeURI(location.search.split('=')[1]) !== 'undefined') {
    var city = decodeURI(location.search.split('=')[1]);
    $('.currentCity').html(city);
    geocoder(city);
  } else {
    AMap.service(['AMap.CitySearch'], function () {//加载城市查询插件
      var citysearch = new AMap.CitySearch();//实例化城市查询
      citysearch.getLocalCity(function (status, result) {//自动获取用户IP，返回当前城市
        if (status === 'complete' && result.info === 'OK' && result.city) {
          $('.currentCity').html(result.city);
          geocoder(result.city);
        } else {
          $('.currentCity').html('北京');
          geocoder('北京');
        }
      });
    });
  }
});

/*main*/
$(function () {
  'use strict';
  var CLICK = 'click';
  if (/(iPhone|iPad|iOS)/i.test(navigator.userAgent)) {
    CLICK = 'touchstart';
  }
  var positionPage = $('#positionPage'),
    weatherTop = $('.weather-top'),
    mask = $('.mask'),
    clicked = false;
  var switchPanel = function () {
    if (!clicked) {
      $.hotCity();
      mask.css({
        'display': 'block',
        'z-index': 4
      });
      positionPage.css({'display': 'block'});
      positionPage.animate({
        opacity: 1
      }, 1, 'linear', function () {
        clicked = true;
      });
      $('span.icon-location').removeClass('icon-mlogfont-switch').addClass('icon-mlogfont-delete');
    }
  };
  var deletePanel = function () {
    if (clicked) {
      mask.css({
        'display': 'none',
        'z-index': 2
      });
      positionPage.animate({
        opacity: 0
      }, 1, 'linear', function () {
        positionPage.css({'display': 'none'});
        clicked = false;
      });
      $('span.icon-location').removeClass('icon-mlogfont-delete').addClass('icon-mlogfont-switch');
    }
  };
  weatherTop.on('click', '.icon-mlogfont-switch', switchPanel);
  weatherTop.on('click', '.icon-mlogfont-delete', deletePanel);
  mask.off('click', '**');
  mask.on('click', deletePanel);
  $('.current-city,.hotcitys').bind(CLICK, function () {
    positionPage.animate({
      opacity: 0
    }, 500);
  });

  var inputCityName = $('#inputCityName');
  inputCityName.bind('keyup', function () {
    var jqueryInput = $(this);
    var searchText = jqueryInput.val();
    var html = '', cityName;
    if ((inputCityName.val() === undefined) || (inputCityName.val().length === 0)) {
      $('#soResult').html(html);
      $('#listHotCitys').css('display', 'block');
      $('#resultPanel').css('display', 'none');
      $('.hotCitys').html('热门城市');
      return;
    }
    $.searchAjax($.config.apiHost + $.config.apiSearch + searchText,
      function () {
        $('#loading').show();
      },
      function (data) {
        $('#loading').hide();
        if (data.length > 0) {
          for (var i = data.length; i > 0; i--) {
            cityName = data[i - 1].county;//data[i - 1].city + ' ' + data[i - 1].county
            html += $.sprintf('<li class="hotcitys"><a href="index.html?area=%s">%s</a></li>', cityName, cityName);
          }
        } else {
          html = '<li class="hotcitys"><a href="javascript:">无匹配城市</a></li>';
        }
        $('#soResult').html(html);
        $('#listHotCitys').css('display', 'none');
        $('#resultPanel').css('display', 'block');
        $('.hotCitys').html('热门区域');
      }
    );
  });
  $.extend({
    'clickAction': function (select, isClose, hasBro, data) {
      var $select = $('.' + select);
      var open = function () {
        var selectClass = select + '-active';

        if (hasBro) {
          $(this).addClass(selectClass).siblings().removeClass(selectClass);
          $('.canvasWrap > .canvasDetail').eq($(this).index()).addClass('canvasDetail-active').siblings().removeClass('canvasDetail-active');
        } else {
          //console.log(data[$(this).index()]);
          mask.css({
            'display': 'block',
            'z-index': 2
          });
          var $selescts = [$('#temp24'), $('#aqi24'), $('#humidity24'), $('#wind24')];
          $.drawLine($selescts, data[$(this).index()]);
          $(this).addClass('daily-active').siblings().removeClass('daily-active').addClass('opacity-hidden');
          $('.daily-active > div').css('opacity', '0');
          $('.daily-detail').addClass('daily-detail-active').siblings().removeClass('daily-detail-active');
          $('.strip_close').addClass('strip_close-active').siblings().removeClass('strip_close-active');
        }
      };
      var close = function () {
        mask.css({
          'display': 'none'
        });
        $('.daily-detail').removeClass('daily-detail-active');
        $('.strip_close').removeClass('strip_close-active');
        $('.daily-active > div').css('opacity', '1');
        $('.daily').removeClass('daily-active opacity-hidden');
        //$('.list-inline').css('opacity', 1);
        $('.canvasSwitch').eq(0).addClass('canvasSwitch-active').siblings().removeClass('canvasSwitch-active');
        $('.canvasDetail').eq(0).addClass('canvasDetail-active').siblings().removeClass('canvasDetail-active');
      };
      if (isClose) {
        $select.on('click', close);
      } else {
        $select.on('click', open);
        mask.off('click', '**');
        mask.on('click', close);
      }
    }
  });
});


/* render */
$(function () {
  'use strict';
  $.extend({
    'hotCity': function () {
      //$('#resultPanel').css('visibility', 'hidden');
      var html = '';
      var citys = {
        '北京': {'y': '116.4', 'x': '39.9'},
        '上海': {'y': '121.47', 'x': '31.23'},
        '广州': {'y': '113.27', 'x': '23.13'},
        '深圳': {'y': '114.05', 'x': '22.55'},
        '天津': {'y': '117.2', 'x': '39.12'},
        '哈尔滨': {'y': '126.53', 'x': '45.8'},
        '长沙': {'y': '112.93', 'x': '28.23'},
        '拉萨': {'y': '91.13', 'x': '29.65'},
        '厦门': {'y': '118.08', 'x': '24.48'},
        '重庆': {'y': '106.55', 'x': '29.57'},
        '杭州': {'y': '120.15', 'x': '30.28'},
        '成都': {'y': '104.07', 'x': '30.67'},
        '长春': {'y': '125.32', 'x': '43.9'},
        '沈阳': {'y': '123.43', 'x': '41.8'},
        '南宁': {'y': '108.37', 'x': '22.82'},
        '合肥': {'y': '117.25', 'x': '31.83'},
        '郑州': {'y': '113.62', 'x': '34.75'},
        '武汉': {'y': '114.3', 'x': '30.6'},
        '南京': {'y': '118.78', 'x': '32.07'},
        '乌鲁木齐': {'y': '87.62', 'x': '43.82'},
        '呼和浩特': {'y': '111.73', 'x': '40.83'},
        '济南': {'y': '116.98', 'x': '36.67'},
        '太原': {'y': '112.55', 'x': '37.87'},
        '石家庄': {'y': '114.52', 'x': '38.05'},
        '西宁': {'y': '101.78', 'x': '36.62'},
        '西安': {'y': '108.93', 'x': '34.27'},
        '兰州': {'y': '103.82', 'x': '36.07'}
      };
      for (var i in citys) {
        html += $.sprintf('<li class="hotcitys"><a href ="index.html?area=%s">%s</a></li>', i, i);
      }
      $('#listHotCitys').html(html);
    },
    'renderNow': function (data) {
      $('.release').html($.releaseFormat(data.upTime));
      $('#weather-num').html(data.tmp);
      $('.weather-num-deg').html('°');
      $('.weather-img-colorful').attr('src', 'images/wicon/weather' + data.wcode + '.png').attr('alt', data.wcn);
      $('.detail-air-icon').addClass('mlogicon icon-mlogfont-aqigood');
      $('.detail-air-desc').html($.fixAqi(data.tip_aqi) + ' ');
      $('.detail-air-value').html(data.aqi);
      $('.detail-wind-icon').addClass('mlogicon icon-mlogfont-wind');
      $('.detail-wind-desc').html(data.wdirDesc + ' ');
      $('.detail-wind-value').html(data.wind);
      $('.detail-humidity-icon').addClass('mlogicon icon-mlogfont-humidity');
      $('.detail-humidity-desc').html('湿度');
      $('.detail-humidity-value').html(data.hum + '%');

    },
    //todo @update template---> handlebar ejs jade ...
    'renderWeek': function (data) {
      var html, am, i;
      html = '';
      am = ((new Date()).getHours() <= 12);
      for (i in data) {
        html += $.sprintf('<li class="daily"><div class="week-summary"><div class="weekday">%s</div><div class="weather-icon %s"></div><div class="weather-temp">%s</div><div class="weather-wind">%s</div><div class="air-desc">%s</div></div></li>', $.weekTimeFormat(data[i].time), (am ? 'icon-iconfont-weather' + data[i].wcode_am : 'icon-iconfont-weather' + data[i].wcode_pm), Math.ceil(data[i].tmax) + '°~' + Math.ceil(data[i].tmin) + '°', data[i].wclass, $.fixAqi(data[i].tip_aqi));
      }
      $('.weather-week .list-inline').html(html);
      $.clickAction('canvasSwitch', false, true);
      $.clickAction('strip_close', true, false);
    },
    'drawLine': function ($selects, data24) {
      var date24Single = [
        {
          series: [{
            name: '温度',
            data: [[data24[0].tip_st, data24[0].st], [data24[4].tip_st, data24[4].st], [data24[8].tip_st, data24[8].st], [data24[12].tip_st, data24[12].st], [data24[16].tip_st, data24[16].st], [data24[20].tip_st, data24[20].st], [data24[24].tip_st, data24[24].st]]
          }],
          tooltip: {
            valueSuffix: '°C',
            headerFormat: '{point.key}<br>',
            pointFormat: '{series.name}：{point.y}',
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            borderWidth: 0,
            shadow: false,
            style: {
              color: 'rgba(255, 255, 255, 0.8)',
              fontSize: '12px',
              fontWeight: 'normal'
            }
          },
          yAxis: {
            lineColor: '#fff',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#fff',
            gridLineColor: '#fff',
            gridLineDashStyle: 'longdash',
            labels: {style: {color: '#fff'}},
            title: {
              text: '温度 (°C)',
              style: {
                color: '#fff'
              }
            }
          }
        },
        {
          series: [{
            name: 'AQI',
            data: [[data24[0].tip_aqi, data24[0].aqi], [data24[4].tip_aqi, data24[4].aqi], [data24[8].tip_aqi, data24[8].aqi], [data24[12].tip_aqi, data24[12].aqi], [data24[16].tip_aqi, data24[16].aqi], [data24[20].tip_aqi, data24[20].aqi], [data24[24].tip_aqi, data24[24].aqi]]
          }],
          tooltip: {
            valueSuffix: '',
            headerFormat: '{point.key}<br>',
            pointFormat: '{series.name}：{point.y}',
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            borderWidth: 0,
            shadow: false,
            style: {
              color: 'rgba(255, 255, 255, 0.8)',
              fontSize: '12px',
              fontWeight: 'normal'
            }
          },
          yAxis: {
            lineColor: '#fff',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#fff',
            gridLineColor: '#fff',
            gridLineDashStyle: 'longdash',
            labels: {style: {color: '#fff'}},
            title: {
              text: '空气质量 (AQI)',
              style: {
                color: '#fff'
              }
            }
          }
        },
        {
          series: [{
            name: '湿度',
            data: [[data24[0].tip_hum, data24[0].hum], [data24[4].tip_hum, data24[4].hum], [data24[8].tip_hum, data24[8].hum], [data24[12].tip_hum, data24[12].hum], [data24[16].tip_hum, data24[16].hum], [data24[20].tip_hum, data24[20].hum], [data24[24].tip_hum, data24[24].hum]]
          }],
          tooltip: {
            valueSuffix: '%',
            headerFormat: '{point.key}<br>',
            pointFormat: '{series.name}：{point.y}',
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            borderWidth: 0,
            shadow: false,
            style: {
              color: 'rgba(255, 255, 255, 0.8)',
              fontSize: '12px',
              fontWeight: 'normal'
            }
          },
          yAxis: {
            lineColor: '#fff',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#fff',
            gridLineColor: '#fff',
            gridLineDashStyle: 'longdash',
            labels: {style: {color: '#fff'}},
            title: {
              text: '湿度 (%)',
              style: {
                color: '#fff'
              }
            }
          }
        },
        {
          series: [{
            name: '风速',
            data: [[data24[0].wdirdesc, data24[0].ws], [data24[4].wdirdesc, data24[4].ws], [data24[8].wdirdesc, data24[8].ws], [data24[12].wdirdesc, data24[12].ws], [data24[16].wdirdesc, data24[16].ws], [data24[20].wdirdesc, data24[20].ws], [data24[24].wdirdesc, data24[24].ws]]
          }],
          tooltip: {
            valueSuffix: 'm/s',
            headerFormat: '风向：{point.key}<br>',
            pointFormat: '{series.name}：{point.y}',
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            borderWidth: 0,
            shadow: false,
            style: {
              color: 'rgba(255, 255, 255, 0.8)',
              fontSize: '12px',
              fontWeight: 'normal'
            }
          },
          yAxis: {
            lineColor: '#fff',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#fff',
            gridLineColor: '#fff',
            gridLineDashStyle: 'longdash',
            labels: {style: {color: '#fff'}},
            title: {
              text: '风速 (m/s)',
              style: {
                color: '#fff'
              }
            }
          }
        }
      ];
      var opt = {
        colors: ['#ffffff', '#f7a35c', '#90ee7e', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          renderTo: 'container',
          type: 'spline',
          backgroundColor: null,
          height: 180,
          width: 680
        },
        title: {
          text: null
        },
        legend: {
          enabled: false
        },
        credits: {
          text: null
        },
        exporting: {
          enabled: false
        },
        xAxis: {
          categories: ['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00'],
          lineColor: '#fff',
          tickColor: '#fff',
          labels: {style: {color: '#fff'}},
          title: {
            style: {
              color: '#fff'
            }
          }
        }
      };
      var len = $selects.length;
      for (var i = 0; i < len; i++) {
        $selects[i].highcharts($.extend({}, opt, date24Single[i]));
      }
    }
  });

});
