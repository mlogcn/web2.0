/**
 * Created by fuchao on 15/8/3.
 */
/*global $*/
$(function () {
  'use strict';
  $.extend({
    'hotCity': function () {
      $('#resultPanel').css('visibility', 'hidden');
      var html = '';
      var citys = {
        '北京': {'y': '116.4', 'x': '39.9'},
        '上海': {'y': '121.47', 'x': '31.23'},
        '广州': {'y': '113.27', 'x': '23.13'},
        '深圳': {'y': '114.05', 'x': '22.55'},
        '厦门': {'y': '118.08', 'x': '24.48'},
        '长春': {'y': '125.32', 'x': '43.9'},
        '沈阳': {'y': '123.43', 'x': '41.8'},
        '哈尔滨': {'y': '126.53', 'x': '45.8'},
        '南宁': {'y': '108.37', 'x': '22.82'},
        '合肥': {'y': '117.25', 'x': '31.83'},
        '杭州': {'y': '120.15', 'x': '30.28'},
        '长沙': {'y': '112.93', 'x': '28.23'},
        '重庆': {'y': '106.55', 'x': '29.57'},
        '郑州': {'y': '113.62', 'x': '34.75'},
        '武汉': {'y': '114.3', 'x': '30.6'},
        '南京': {'y': '118.78', 'x': '32.07'},
        '乌鲁木齐': {'y': '87.62', 'x': '43.82'},
        '呼和浩特': {'y': '111.73', 'x': '40.83'},
        '天津': {'y': '117.2', 'x': '39.12'},
        '济南': {'y': '116.98', 'x': '36.67'},
        '太原': {'y': '112.55', 'x': '37.87'},
        '石家庄': {'y': '114.52', 'x': '38.05'},
        '西宁': {'y': '101.78', 'x': '36.62'},
        '西安': {'y': '108.93', 'x': '34.27'},
        '成都': {'y': '104.07', 'x': '30.67'},
        '拉萨': {'y': '91.13', 'x': '29.65'},
        '兰州': {'y': '103.82', 'x': '36.07'}
      };
      for (var i in citys) {
        html += $.sprintf('<li class="hotcitys"><a href ="index.html?area=%s">%s</a></li>', i, i);
      }
      $('#listHotCitys').html(html);
    },
    'renderNow': function (data) {
      $('#currentCity').html('切换城市');
      $('.release').html($.releaseFormat(data.upTime));
      $('#wicon-num').html(data.tmp);
      $('.weather-num-deg').html('°');
      $('.wicon-icon-colorful').addClass('wicon' + data.wcode);
      $('.detail-air-desc').html($.fixAqi(data.tip_aqi) + ' ');
      $('.detail-air-value').html(data.aqi);
      $('.detail-wind-desc').html(data.wdirDesc + ' ');
      $('.detail-wind-value').html(data.wind);
      $('.detail-weat-desc').html(data.tip_st + ' 湿度');
      $('.detail-weat-value').html(data.hum);
    },
    'renderWeek': function (data) {
      var html, am, i;
      html = '';
      am = ((new Date()).getHours() <= 12);
      for (i in data) {
        html += $.sprintf('<li><div class="weekday">%s</div> <div class="wicon-icon %s">xxx</div> <div class="wicon-temp">%s</div> <div class="wicon-desc">%s</div> <div class="air-desc">%s</div> </li>', $.weekTimeFormat(data[i].time), (am ? data[i].wcode_am : data[i].wcode_pm), Math.ceil(data[i].tmax) + '°~' + Math.ceil(data[i].tmin) + '°', (am ? data[i].w_am : data[i].w_pm), $.fixAqi(data[i].tip_aqi));
      }
      $('.wicon-week .list-inline').html(html);
      navigator.geolocation.getCurrentPosition(function (position) {
        $('#currentCity').html($.getGeo(position.coords.latitude, position.coords.longitude));
      });
    }
  });
});
