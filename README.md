#Mlog website v2.0#

```
npm install && bower install
```

**IDE** 

Webstorm is recommended

more info in the package.json

**important**

before push -->
run && check out the error

```
gulp build
```

#todo

1. 新闻页面及其编辑后台
2. 天气应用轮播及其替换
3. 页脚样式优化,跟整体风格有些不合,按照设计稿细节进行修改
4. 移动端适配细节调整
5. *升级时,需要手动将favicon 目录添加到部署的根目录上,或者在未来升级页面的时候,使用覆盖,而非删除后再添加.*
6. 对图片进行压缩及精灵化
7. 升级最新数据接口
